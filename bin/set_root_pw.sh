#!/bin/bash

if [ -f ~/.${HOSTNAME}-pwd ]; then
	echo "Root password already saved. Using this one"
	PASS=$(<~/.${HOSTNAME}-pwd)
else
	PASS=${ROOT_PASS:-$(pwgen -s 12 1)}
	_word=$( [ ${ROOT_PASS} ] && echo "preset" || echo "random" )
	echo "=> Setting a ${_word} password to the root user"
fi
echo "root:$PASS" | chpasswd
echo "=> Done!"
echo $PASS > ~/.${HOSTNAME}-pwd
IP=$(hostname --ip-address)
echo "========================================================================"
echo "     You can now connect to this Developer Container via SSH using:"
echo ""
echo "                      ssh -p <port> root@${IP}"
echo ""
echo "            and enter the password '$PASS' when prompted"
echo ""
echo "========================================================================"
echo "§§PWD:$PASS"
